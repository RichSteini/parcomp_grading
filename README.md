# Parallel Computing Grading Script

This script aims to simplify the grading process for exercise sheet grading which is structured
in exercises with distinct sub-exercises (or "answers", if you will).

## Configuration

The parameters of the script (mainly sheet name and sheet composition) can be changed in the
configuration section of the script (top of the file just under the package imports). 

## Requirements

The script takes one (optional) argument `-e` that specifies the range of
exercises that should be graded in the format `FIRSTEX-LASTEX` (e.g. `-e
1-4`). *For every exercise*, there must be a corresponding CSV file named
`SHEETNAME_EXNUM.csv`, where `SHEETNAME` is the sheet name set in the
configuration section and `EXNUM` a two-digit decimal number for the exercise
number (for example, if `SHEETNAME=blatt1` and the file describes exercise 1,
the file should be called `blatt1_01.csv`).

These CSV files are expected to have the following format:

```csv
"exercise","group","student_1","student_2","points","feedback"
1,"Group 1","Frank Zappa",NA,NA,""
1,"Group 2","Ruth Underwood","George Duke",3,"@1@0@correct
@2@0@correct
@3@-0.5@j should be private"
```

Note that quotes are only required if the column spans over multiple rows.
CSV files should also contain the same "keys", i.e. student and group names.
NA marks unassigned student_2 and points fields. The feedback field accepts 
a string matching the regular expression `@([0-9]+)@(-?[0-9]*(\.[0-9]+)?)@(.*)`.
The first number is the sub-exercise number, the second one the number of points that are deducted. 

The script also supports opening PDF files for each group, these have to be contained in
a folder `pdf/` and must be named `GROUPNAME.pdf`, where `GROUPNAME` is the name of
the group the PDF was submitted by (PDF's can be exported in this format from Moodle/TUWEL).

## Saving and exporting

The script exports the same file type that can be read, using the `Write to csv [w]` feature in
the main menu. Furthermore, after grading of every exercise and on exit with `Ctrl+C` (which is
possible in any menu prompt) autosaves will be created for common deductions and the currently
assigned grades (files `.SHEETNAME_autosave.csv` and `.sheetname_deductions.csv`, respectively).

## Limitations

The following limitations apply in the current version (if needed, this can be addressed):

- Only subsequent exercises can be selected, i.e. there must be a
corresponding CSV file for each element in the selected exercise range
- The script will probably not function correctly in Windows terminals (not tested)
- There is a maximum of 15 Exercises per sheet (such that they can be selected with a hex digit from the group view)
- The script only works correctly if the directory it is executed in contains the exercise CSV files