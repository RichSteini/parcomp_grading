#!/usr/bin/python3 -u

import csv
import argparse
import re
import os
import readchar
import webbrowser
import readline
import subprocess

# ========== CONFIGURATION ========== #

SHEETNAME = "blatt1"

def init_sheet():
    sh = Sheet()

    # Replace with exercise sheet data
    # syntax: Exercise(num,answers,points)
    sh.exercises.append(Exercise(1,3,3))
    sh.exercises.append(Exercise(2,4,4))
    sh.exercises.append(Exercise(3,3,3))
    sh.exercises.append(Exercise(4,2,4))
    sh.exercises.append(Exercise(5,3,3))
    sh.exercises.append(Exercise(6,2,2))
    sh.exercises.append(Exercise(7,2,4))
    sh.exercises.append(Exercise(8,2,4))
    sh.exercises.append(Exercise(9,4,6))
    sh.exercises.append(Exercise(10,4,4))
    return sh

# ========== CONSTANTS ========== #

EX_PATTERN = "([0-9]+)-([0-9]+)"
CSV_BASENAME = SHEETNAME+"_{:02d}.csv"
CSV_PATTERN = SHEETNAME+"_([0-9][0-9]).csv"
AUTOSAVE_FILENAME = "."+SHEETNAME+"_autosave.csv"
DEDUCTION_FILE = "."+SHEETNAME+"_deductions.csv"

NA = "NA"

FEEDBACK_PATTERN = "@([0-9]+)@(-?[0-9]*(\.[0-9]+)?)@(.*)"

CSV_FIRSTLINE = "exercise,group,student_1,student_2,points,feedback\n"
DEDUCTIONS_FIRSTLINE = "ex_num,ans_num,reason,points,count\n"

DEFAULT_FIRSTEX = 1
DEFAULT_LASTEX = 16

GRADED = 0
PARTIALLY_GRADED = 1
UNGRADED = 2

PDF_PATH = "pdf/"
PDF_VIEWERS= ["evince", "xdg-open", "okular"]

# ========== DATA STRUCTURE ========== #

class Exercise:
    def __init__(self, ex_num, num_answ, max_points):
        self.ex_num = ex_num
        self.answers = []
        self.num_answ = num_answ
        for i in range(0,num_answ):
            x = Answer(i+1)
            self.answers.append(x)
        self.max_points = max_points
    
    def deduct(self, answer_num, reason, points):
        if answer_num > self.num_answ:
            print("Invalid Answer")
            return
        self.answers[answer_num - 1].deduct(reason, points)
    
    def _is_graded(self):
        ans_status = []
        for a in self.answers:
            ans_status.append(a.graded)
        if not True in ans_status:
            return UNGRADED
        else:
            if not False in ans_status:
                return GRADED
            else: 
                return PARTIALLY_GRADED
    
    def mark_correct(self, ans_num):
        self.answers[ans_num-1].mark_correct()
    
    def _calc_points(self):
        p = self.max_points
        for a in self.answers:
            p += a.deductions.deducted_points()
        if p < 0:
            p = 0
        return p
    
    graded = property(_is_graded)
    act_points = property(_calc_points)
    
    def csv_string(self, group, student_1, student_2, exercise=None):
        # enquote student_2 correctly
        if student_2 != NA:
            student_2 = "\"" + student_2 + "\""
        if exercise is None or self.ex_num in exercise:
            if self.graded == GRADED:
                fb_str = ""
                for ans in self.answers:
                    fb_str += ans.csv_feedback_string()
                # remove last newline
                if fb_str != "":
                    fb_str = fb_str[:-1]
                return "{},\"{}\",\"{}\",{},{},\"{}\"\n".format(self.ex_num, group, student_1, student_2, self.act_points, fb_str)
            else:
                return "{},\"{}\",\"{}\",{},{},\"{}\"\n".format(self.ex_num, group, student_1, student_2, NA, "")
        else:
            return ""
    
    def short_rep(self):
        return "Exercise {} ({})".format(self.ex_num, self._grade_string())

    def _grade_string(self):
        grade_string = "not graded"
        if self.graded == GRADED:
            grade_string = "{}/{} Points".format(self.act_points, self.max_points)
        elif self.graded == PARTIALLY_GRADED:
            grade_string = "partially graded"
        return grade_string

    def long_rep(self):
        rep = "Exercise {} ({})\n\n".format(self.ex_num, self._grade_string())
        for a in self.answers:
            rep += a.rep(self.ex_num, key=True)
        return rep

    def html_string(self):
        s = "<p>\n<b>Exercise {} (max {}P): {}P</b><ul>".format(self.ex_num, self.max_points, self.act_points)
        for answer in self.answers:
            s += answer.html_string(self.ex_num) + "\n"
        s += "</ul></p>"
        return s
       

class Answer:
    def __init__(self, ans_num):
        self.num = ans_num
        self.deductions = Deductions(ans_num)
        self.graded = False
    
    def deduct(self, reason, points):
        self.deductions.add(reason, points)
        self.graded = True
    
    def mark_correct(self):
        self.deductions.clear()
        self.graded = True
    
    def html_string(self, ex_num):
        return "<li>{}.{}: {}</li>".format(ex_num, self.num, self.deductions.html_string())
    
    def feedback_string(self):
        return self.deductions.feedback_string()
    
    def csv_feedback_string(self):
        return self.deductions.csv_feedback_string()
    
    def rep(self, ex_num, key=False):
        repstr = ""
        if key:
            repstr += "\t[{}] ".format(self.num)
        grade_string = "not graded"
        if self.graded:
            grade_string = self.deductions.rep()
        repstr += "{}.{}: {}\n".format(ex_num, self.num, grade_string)
        return repstr

class Sheet:
    def __init__(self):
        self.exercises = []

    def html_string(self):
        s = ""
        for e in self.exercises:
            s += e.html_string() + "\n"
        max_points = 0
        reached_points = 0
        for e in self.exercises:
            max_points += e.max_points
            reached_points += e.act_points
        s += "<p><b>Exercise Sheet Total (max {}P): {}P</b></p>\n".format(max_points, reached_points)
        return s
    
    def print_html(self):
        print(self.html_string())


    def deduct(self, ex_num, answer_num, gradetext, deduction):
        self.exercises[ex_num-1].deduct(answer_num, gradetext, deduction)
    
    def mark_correct(self, ex_num, ans_num):
        self.exercises[ex_num-1].mark_correct(ans_num)
    
    def grade_status(self, exercise=None):
        ex_status = []
        for e in self.exercises:
            if exercise is None or e.ex_num in exercise:
                ex_status.append(e.graded)
        if not (GRADED in ex_status or PARTIALLY_GRADED in ex_status):
            return UNGRADED
        else:
            if not (PARTIALLY_GRADED in ex_status or UNGRADED in ex_status):
                return GRADED
            else: 
                return PARTIALLY_GRADED
    
    def csv_string(self, group, student_1, student_2, exercise=None):
        s = ""
        for e in self.exercises:
            s += e.csv_string(group, student_1, student_2, exercise=exercise)
        return s
    
    def filter_exercises(self, first_ex, last_ex):
        new_exs = []
        for e in self.exercises:
            if e.ex_num>=first_ex and e.ex_num<=last_ex:
                new_exs.append(e)
        self.exercises = new_exs

class Deductions:
    def __init__(self, ans_num):
        self.deds = []
        self.ans_num = ans_num

    def add(self, reason, points):
        self.deds.append((reason, points))
    
    def feedback_string(self):
        s = ""
        if len(self.deds) == 0:
            s += "-{}: correct\n".format(self.ans_num)
        else:
            for d in self.deds:
                if d[1] == 0:
                    s += "-{}: {}\n".format(self.ans_num, d[0])
                else:
                    s += "-{}: {} ({}P)\n".format(self.ans_num, d[0], d[1])
            s = s[:-1]
        return s

    def csv_feedback_string(self):
        s = ""
        if len(self.deds) == 0:
            s += "@{}@0@correct\n".format(self.ans_num)
        else:
            for d in self.deds:
                s += "@{}@{}@{}\n".format(self.ans_num, d[1], d[0])
        return s

    def html_string(self):
        if len(self.deds) == 0:
            return "correct"
        elif len(self.deds) == 1:
            if self.deds[0][1] == 0:
                return self.deds[0][0]
            else:
                return "{} ({}P)".format(self.deds[0][0], self.deds[0][1])
        else:
            s = "<ul>\n"
            for d in self.deds:
                if d[1] == 0:
                    s += "<li>{}</li>".format(d[0])
                else:
                    s += "<li>{} ({}P)</li>\n".format(d[0], d[1])
            s += "</ul>"
            return s
        
    def rep(self):
        if len(self.deds) == 0:
            return "correct"
        elif len(self.deds) == 1:
            if self.deds[0][1] == 0:
                return self.deds[0][0]
            else:
                return "{} ({}P)".format(self.deds[0][0], self.deds[0][1])
        else:
            s = "\n"
            for d in self.deds:
                if d[1] == 0:
                    s += "\t\t- {}\n".format(d[0])
                else:
                    s += "\t\t- {} ({}P)\n".format(d[0], d[1])
            s = s[:-1]
            return s
    
    def deducted_points(self):
        p = 0
        for d in self.deds:
            p += float(d[1])
        return p

    def clear(self):
        self.deds.clear()

class FrameEntry:
    def __init__(self,group,student_1,student_2):
        self.group = group
        self.student_1 = student_1
        self.student_2 = student_2
        self.sheet = init_sheet()
    
    def parse_feedback(self, deds, ex_num, points, feedback):
        if points != NA:
            for fb in feedback.split("\n"):
                l = re.finditer(FEEDBACK_PATTERN, fb)
                for m in l:
                    ans_num = m[1]
                    deduction = m[2]
                    grade_text = m[4]
                    if float(deduction) == 0 and re.match("\s*correct\s*", grade_text):
                        self.sheet.mark_correct(int(ex_num), int(ans_num))
                    else:
                        self.sheet.deduct(int(ex_num), int(ans_num), grade_text, float(deduction))
                        deds.add_entry(int(ex_num), int(ans_num), grade_text, float(deduction))
    
    def csv_string(self, exercise=None):
        return self.sheet.csv_string(self.group, self.student_1, self.student_2, exercise=exercise)
    
    def group_string(self):
        students = ""
        if self.student_1 != NA:
            students += self.student_1
            if self.student_2 != NA:
                students += ", "
                students += self.student_2
        return "{} ({})".format(self.group, students)
        

class Frame:
    def __init__(self, first_ex, last_ex):
        self.first_ex = int(first_ex)
        self.last_ex = int(last_ex)

        # get specified csv files
        potential_files = [CSV_BASENAME.format(i) for i in range(int(first_ex),int(last_ex)+1)]
        d = os.listdir()
        files = list(set(potential_files) & set(d))

        # revise firstex and lastex based on avaliable files
        exs = []
        for f in files:
            m = re.search(CSV_PATTERN, f)
            exs.append(int(m.groups(1)[0]))
        
        
        if min(exs) != self.first_ex:
            print("INFO: First specified exercise ({}) is smaller than minimum csv file found ({}), setting first exercise to {}".format(self.first_ex, min(exs), min(exs))) 
            self.first_ex = min(exs)
        if max(exs) != self.last_ex:
            print("INFO: Last specified exercise ({}) is larger than maximum csv file found ({}), setting last exercise to {}".format(self.last_ex, max(exs), max(exs)))
            self.last_ex = max(exs)

        # check if deductions file exists and create if not present
        if not os.path.isfile(DEDUCTION_FILE):
            print("INFO: No deduction file found, creating new one")
            with open(DEDUCTION_FILE, "w") as f:
                f.write(DEDUCTIONS_FIRSTLINE)
        
        self.deds = DeductionCollection()

        self.data = {}

        if AUTOSAVE_FILENAME in os.listdir():
            print("INFO: Restoring from autosave, delete \"{}\" if you want to start from scratch\n".format(AUTOSAVE_FILENAME))
            # create data structure from first file
            with open(AUTOSAVE_FILENAME, "r") as f:
                reader = csv.reader(f, delimiter=",")
                for (i, line) in enumerate(reader):
                    if i != 0:
                        group_name = line[1]
                        student_1 = line[2]
                        student_2 = line[3]
                        if group_name not in self.data.keys():
                            self.data[group_name] = FrameEntry(group_name, student_1, student_2)
                        self.data[group_name].parse_feedback(self.deds, line[0], line[4], line[5])
        else:
            print("INFO: No autosave found, starting from scratch.\n")
            # create data structure from first file
            with open(files[0], "r") as f:
                reader = csv.reader(f, delimiter=",")
                for (i, line) in enumerate(reader):
                    if i != 0:
                        group_name = line[1]
                        student_1 = line[2]
                        student_2 = line[3]
                        self.data[group_name] = FrameEntry(group_name, student_1, student_2)
                        self.data[group_name].parse_feedback(self.deds, line[0], line[4], line[5])

            # read other files
            for f in files[1::]:
                with open(f, "r") as f:
                    reader = csv.reader(f, delimiter=",")
                    for (i, line) in enumerate(reader):
                        if i != 0:
                            group_name = line[1]
                            self.data[group_name].parse_feedback(self.deds, line[0], line[4], line[5])
        

    def csv_string(self, exercise=None):
        s = ""
        for k in self.data.values():
            s += k.csv_string(exercise=exercise) 
        return s
    
    def write_csv_files(self):
        basename = CSV_BASENAME

        for e in range(self.first_ex, self.last_ex+1):
            os.rename(basename.format(e), basename.format(e)+".old")

        for e in range(self.first_ex, self.last_ex+1):
            with open(basename.format(e), "w") as f:
                f.write(CSV_FIRSTLINE)
                f.write(self.csv_string(exercise=[e]))
    
    def autosave(self):
        filename = AUTOSAVE_FILENAME
        with open(filename, "w") as f:
            f.write(CSV_FIRSTLINE)
            f.write(self.csv_string())
        
        self.deds.write_csv()
    
    def ungraded_groups(self):
        return list(filter(lambda e: e.sheet.grade_status(exercise=range(self.first_ex, self.last_ex+1)) == UNGRADED, self.data.values()))

    def partial_groups(self):
        return list(filter(lambda e: e.sheet.grade_status(exercise=range(self.first_ex, self.last_ex+1)) == PARTIALLY_GRADED, self.data.values()))

    def graded_groups(self):
        return list(filter(lambda e: e.sheet.grade_status(exercise=range(self.first_ex, self.last_ex+1)) == GRADED, self.data.values()))

class DeductionCollection:
    def __init__(self):
        self.entries = []
        with open(DEDUCTION_FILE, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for (i, line) in enumerate(reader):
                if i != 0:
                    ex_num = line[0]
                    ans_num = line[1]
                    reason = line[2]
                    points = line[3]
                    count = line[4]
                    self.new_entry(ex_num, ans_num, reason, points, count)
    
    def add_entry(self, ex_num, ans_num, reason, points):
        found = False
        for e in self.entries:
            if e.ex_num == ex_num and e.ans_num == ans_num and \
                e.reason == reason and e.points == points:
                e.increment_count()
                found = True
                break
        if not found:
            self.new_entry(ex_num, ans_num, reason, points, 1)
        

    def new_entry(self, ex_num, ans_num, reason, points, count):
        self.entries.append(DeductionEntry(ex_num, ans_num, reason, points, count))
    
    def write_csv(self):
        with open(DEDUCTION_FILE, "w") as f:
            f.write(DEDUCTIONS_FIRSTLINE)
            for d in self.entries:
                f.write(d.csv_string())

    def get(self, ex_num, ans_num):
        deds = list(filter(lambda e: e.ex_num == ex_num and e.ans_num == ans_num, self.entries))
        deds.sort(key=lambda e: e.count, reverse=True)
        return deds
    
    def __str__(self):
        s = ""
        for d in self.entries:
            s += str(d) + "\n"
        return s

class DeductionEntry:
    def __init__(self, ex_num, ans_num, reason, points, count):
        self.ex_num = int(ex_num)
        self.ans_num = int(ans_num)
        self.reason = reason
        self.points = float(points)
        self.count = int(count)
    
    def increment_count(self):
        self.count += 1
    
    def csv_string(self):
        return "{},{},\"{}\",{},{}\n".format(self.ex_num, self.ans_num, self.reason, self.points, self.count)
    
    def __str__(self):
        return "{}.{}: {} ({} Points)".format(self.ex_num, self.ans_num, self.reason, self.points)


# ========== COMMAND LINE LOGIC ========== #

class State:
    def __init__(self, frame):
        self.frame = frame

def group_overview(frame):
    ungraded_groups = frame.ungraded_groups()
    graded_groups = frame.graded_groups()
    partial_groups = frame.partial_groups()

    return "Found {} groups: \n\tGraded: {}\n\tPartially Graded: {}\n\tUngraded: {}\n".format(
        len(frame.data.values()), len(graded_groups), len(partial_groups), len(ungraded_groups))

def clear_terminal():
    print("\033c", end="")

def handle_action(state, actions, message, prompt_string=None):
    keys = []
    descriptions = []
    methods = []

    create_prompt_string = prompt_string == None
    
    # prepare lists and prompt
    if create_prompt_string:
        prompt_string = ""
        ACTION_FORMAT = "{} [{}]"
    for a in actions:
        k, d, m = a
        keys.append(k)
        descriptions.append(d)
        methods.append(m)
        if create_prompt_string:
            prompt_string += ACTION_FORMAT.format(d, k)
            prompt_string += "  "

    c = None
    while c is None:
        print(message, end="")

        #print("\nEnter the corresponding key for the action you want to take:")
        print()
        print(prompt_string)
        c = readchar.readkey()

        # standard key bindings
        if c == readchar.key.CTRL_C:
            state.frame.autosave()
            exit(0)

        clear_terminal()

        matching = []
        for k in keys:
            if c in k:
                matching.append(k)
        if len(matching) > 0:
            methods[keys.index(matching[0])](state)
        else:
            ch = c if c.isalnum() else c.encode()
            print("\nUnknown option: \"{}\"\n".format(ch))
            c = None
    
    return c
    

def menu(frame):
    while True:
        message = "Parallel Computing Grading Script V0.1\n\n"
        message += group_overview(frame)
        actions = [
            ("g", "Quick Grade", act_quick_grade),
            ("s", "Search a group", act_search_group),
            ("w", "Write to .csv files", act_write_output),
            ("x", "Exit", act_exit),
        ]
        state = State(frame)
        handle_action(state, actions, message)

def select_group(state, groups):
    if len(groups) == 0:
        print("No matching group found\n")
    elif len(groups) == 1:
        state.page_groups = groups
        state.selection = 0
        act_group_view(state)
        state.selection = None
        state.page_groups = None
    else:
        group_selection_view(state, groups)

def group_selection_view(state, groups):
    if not hasattr(state, "page") or state.page is None:
        state.page = 0

    actions = [
        ("1", "Select Group 1", act_sel_1),
        ("2", "Select Group 2", act_sel_2),
        ("3", "Select Group 3", act_sel_3),
        ("4", "Select Group 4", act_sel_4),
        ("5", "Select Group 5", act_sel_5),
        ("n"+readchar.key.RIGHT, "Next Page", act_next_page),
        ("p"+readchar.key.LEFT, "Previous Page", act_previous_page),
        ("b" , "Back", act_back)
    ]
    while True:
        state.sel_method = act_group_view

        first_index = state.page*5 % len(groups)
        state.page_groups = groups[first_index % len(groups): (first_index + 5) % len(groups)]
        message = "Select a group:\n\n"
        for i in range(5):
            message += "\t[{}] {}\n".format(i+1, groups[(first_index + i) % len(groups)].group_string())

        prompt_string = "Select Group [1-5]  Next page [->, n]  Previous page [<-, p]  Back [b]"
        act = handle_action(state, actions, message, prompt_string=prompt_string)
        if act == "b":
            break

    state.page = None
    state.page_groups = None
    state.sel_method = None

# ========== ACTIONS ========== #

def act_exit(state):
    state.frame.autosave()
    exit(0)

def act_quick_grade(state):
    groups = state.frame.partial_groups()
    groups.extend(state.frame.ungraded_groups())
    if len(groups) == 0:
        print("All groups are graded\n")
    else:
        select_group(state, groups[0:1])

def act_write_output(state):
    print("Writing data to csv files and renaming existing csv files to FILENAME.old")
    state.frame.write_csv_files()
    print("Successfully wrote data to csv files")

def act_sel_1(state):
    state.selection = 0
    state.sel_method(state)
    state.selection = None

def act_sel_2(state):
    state.selection = 1
    state.sel_method(state)
    state.selection = None

def act_sel_3(state):
    state.selection = 2
    state.sel_method(state)
    state.selection = None

def act_sel_4(state):
    state.selection = 3
    state.sel_method(state)
    state.selection = None

def act_sel_5(state):
    state.selection = 4
    state.sel_method(state)
    state.selection = None

def act_sel_6(state):
    state.selection = 5
    state.sel_method(state)
    state.selection = None

def act_sel_7(state):
    state.selection = 6
    state.sel_method(state)
    state.selection = None

def act_sel_8(state):
    state.selection = 7
    state.sel_method(state)
    state.selection = None

def act_sel_9(state):
    state.selection = 8
    state.sel_method(state)
    state.selection = None

def act_sel_10(state):
    state.selection = 9
    state.sel_method(state)
    state.selection = None

def act_sel_11(state):
    state.selection = 10
    state.sel_method(state)
    state.selection = None

def act_sel_12(state):
    state.selection = 11
    state.sel_method(state)
    state.selection = None

def act_sel_13(state):
    state.selection = 12
    state.sel_method(state)
    state.selection = None

def act_sel_14(state):
    state.selection = 13
    state.sel_method(state)
    state.selection = None

def act_sel_15(state):
    state.selection = 14
    state.sel_method(state)
    state.selection = None

def act_back(state):
    # NO OPERATION
    return

def act_next_page(state):
    if not hasattr(state, "page") or state.page is None:
        state.page = 1
    else:
        state.page += 1

def act_previous_page(state):
    if not hasattr(state, "page") or state.page is None:
        state.page = 0
    else:
        state.page -= 1

def act_next_exc(state):
    if not hasattr(state, "ex_num") or state.ex_num is None:
        state.ex_num = 1
    else:
        state.ex_num += 1

def act_previous_exc(state):
    if not hasattr(state, "ex_num") or state.ex_num is None:
        state.ex_num = 0
    else:
        state.ex_num -= 1

def act_next_ans(state):
    if not hasattr(state, "ans_num") or state.ans_num is None:
        state.ans_num = 1
    else:
        state.ans_num += 1
    state.ded_page = 0

def act_previous_ans(state):
    if not hasattr(state, "ans_num") or state.ans_num is None:
        state.ans_num = 0
    else:
        state.ans_num -= 1
    state.ded_page = 0

def act_search_group(state):
    name = input("Enter search string: ").lower()
    clear_terminal()
    matching_groups = list(filter(lambda e: name in e.group.lower() or name in e.student_1.lower() or name in e.student_2.lower(), state.frame.data.values()))
    select_group(state, matching_groups)

def act_previous_deds(state):
    if not hasattr(state, "ded_page") or state.ded_page is None:
        state.ded_page = 0
    else:
        if state.ded_page > 0:
            state.ded_page -= 1

def act_next_deds(state):
    if not hasattr(state, "ded_page") or state.ded_page is None:
        state.ded_page = 1
    else:
        if (state.ded_page+1)*5 < len(state.deds):
            state.ded_page += 1

def act_open_pdf(state):
    pdf = PDF_PATH + state.selected_group.group + ".pdf"
    if not os.path.isfile(pdf):
        print("No such file: " + pdf + "\n")
    else:
        webbrowser.open_new("file://"+os.getcwd() + "/" + pdf)

def act_open_pdf_viewer(state):
    pdf = PDF_PATH + state.selected_group.group + ".pdf"
    if not os.path.isfile(pdf):
        print("No such file: " + pdf + "\n")
    else:
        for viewer in PDF_VIEWERS:
            cmd = "which " + viewer + " | grep -o " + viewer + " > /dev/null && echo '0' || echo '1'"
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            p_status = p.wait()
            if output == b'0\n':
                subprocess.Popen([viewer, pdf], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                return

        print("Either no supported pdf viewer or 'which' not installed, list of supported pdf viewers:")
        print(*PDF_VIEWERS, sep=", ")
        print("\n")

def act_mark_correct(state):
    state.selected_exc.answers[state.ans_num].mark_correct()
    act_next_ans(state)

def act_new_ded(state):
    reason = input("Enter reason for deduction: ")
    parsed_points = 0
    while True:
        points = input("Enter points that should be deducted: ")
        try:
            parsed_points = float(points)
            if parsed_points > 0:
                parsed_points = -parsed_points
            break
        except ValueError:
            print("Invalid floating point number {}".format(points))
            points = None
    state.frame.deds.add_entry(state.ex_num+1, state.ans_num+1, reason, parsed_points)
    state.selected_exc.deduct(state.ans_num+1, reason, parsed_points)
    clear_terminal()
        

def act_deduct_selected(state):
    if state.ded_page*5+state.selection < len(state.deds):
        ded = state.deds[state.ded_page*5+state.selection] 
        state.selected_exc.deduct(state.ans_num+1, ded.reason, ded.points)
        ded.increment_count()

def act_ans_view(state):
    if not state.selection < len(state.selected_exc.answers):
        print("invalid answer selection ({})\n".format(state.selection+1))
        return
    
    state.ded_page = 0

    actions = [
        ("n"+readchar.key.RIGHT, "Next", act_next_ans),
        ("p"+readchar.key.LEFT, "Previous", act_previous_ans),
        ("1", "Select Deduction 1", act_sel_1),
        ("2", "Select Deduction 2", act_sel_2),
        ("3", "Select Deduction 3", act_sel_3),
        ("4", "Select Deduction 4", act_sel_4),
        ("5", "Select Deduction 5", act_sel_5),
        (readchar.key.UP, "Previous deds", act_previous_deds),
        (readchar.key.DOWN, "Next deds", act_next_deds),
        ("c", "Mark as correct", act_mark_correct),
        ("d", "Add new deduction", act_new_ded),
        ("b", "Back", act_back)
    ]
    prompt_string = "Mark as correct [c]  Choose Deduction [1-5]  Add new deduction [d]  Scroll Deductions [UP, DOWN]"+\
        "\nNext Subexercise [n,->]  Previous Subexercise [p,<-]  Back [b]"

    state.ans_num = state.selection
    state.deds = state.frame.deds.get(state.ex_num+1, state.ans_num+1)
    while True:
        if state.ans_num + 1> len(state.selected_exc.answers):
            if state.ex_num + 2 > state.frame.last_ex:
                return
            else:
                state.ans_num = 0
                state.ex_num += 1
                state.selected_exc = state.selected_group.sheet.exercises[state.ex_num]
                state.frame.autosave()
        if state.ans_num < 0:
            if state.ex_num < state.frame.first_ex:
                return
            else:
                state.ans_num = len(state.selected_group.sheet.exercises[state.ex_num - 1].answers) - 1
                state.ex_num -= 1
                state.selected_exc = state.selected_group.sheet.exercises[state.ex_num]
                state.frame.autosave()
        
        state.deds = state.frame.deds.get(state.ex_num+1, state.ans_num+1)
        answer = state.selected_exc.answers[state.ans_num]

        message = state.selected_group.group_string()+"\n" + state.selected_exc.short_rep() + "\n\n"
        message += "Subexercise "+ answer.rep(state.ex_num + 1)

        state.sel_method = act_deduct_selected

        if len(state.deds) == 0:
            message += "\nNo deductions found\n"
        else:
            message += "\nCommon Deductions:\n" 
            first_ded = state.ded_page*5
            last_ded = min((state.ded_page+1)*5, len(state.deds))
            j = 1
            for i in range(first_ded, last_ded):
                message += "\t[{}] {} ({}P)\n".format(j, state.deds[i].reason, state.deds[i].points)
                j += 1

        act = handle_action(state,actions,message,prompt_string=prompt_string)
        if act == "b":
            break

    state.ded_page = None
    state.deds = None
    state.ans_num = None

def act_exercise_view(state):
    if not state.selection + state.frame.first_ex in range(state.frame.first_ex, state.frame.last_ex + 1):
        print("invalid exercise selection ({})\n".format(state.selection+1))
        return
    
    actions = [
        ("1", "Select Subexercise 1", act_sel_1),
        ("2", "Select Subexercise 2", act_sel_2),
        ("3", "Select Subexercise 3", act_sel_3),
        ("4", "Select Subexercise 4", act_sel_4),
        ("5", "Select Subexercise 5", act_sel_5),
        ("6", "Select Subexercise 6", act_sel_6),
        ("7", "Select Subexercise 7", act_sel_7),
        ("8", "Select Subexercise 8", act_sel_8),
        ("9", "Select Subexercise 9", act_sel_9),
        ("n"+readchar.key.RIGHT, "Next", act_next_exc),
        ("p"+readchar.key.LEFT, "Previous", act_previous_exc),
        ("b", "Back", act_back)
    ]
    prompt_string = "Select Subexercise [1-9]  Next [n,->]  Previous [p,<-]  Back [b]"

    exercises = state.selected_group.sheet.exercises

    state.ex_num = state.selection + state.frame.first_ex - 1
    while True:
        if state.ex_num + 1 > state.frame.last_ex or state.ex_num + 1 < state.frame.first_ex:
            return
        exc = exercises[state.ex_num]
        message = state.selected_group.group_string()+"\n\n"
        message += exc.long_rep()
        state.selected_exc = exc

        state.sel_method = act_ans_view
        act = handle_action(state,actions,message,prompt_string=prompt_string)
        if act == "b":
            state.frame.autosave()
            break


def act_group_view(state):
    selected_group = state.page_groups[state.selection % len(state.page_groups)]
    sheet = selected_group.sheet
    
    state.selected_group = selected_group

    actions = [
        ("1", "Select Exercise 1", act_sel_1),
        ("2", "Select Exercise 2", act_sel_2),
        ("3", "Select Exercise 3", act_sel_3),
        ("4", "Select Exercise 4", act_sel_4),
        ("5", "Select Exercise 5", act_sel_5),
        ("6", "Select Exercise 6", act_sel_6),
        ("7", "Select Exercise 7", act_sel_7),
        ("8", "Select Exercise 8", act_sel_8),
        ("9", "Select Exercise 9", act_sel_9),
        ("A", "Select Exercise 10", act_sel_10),
        ("B", "Select Exercise 11", act_sel_11),
        ("C", "Select Exercise 12", act_sel_12),
        ("D", "Select Exercise 13", act_sel_13),
        ("E", "Select Exercise 14", act_sel_14),
        ("F", "Select Exercise 15", act_sel_15),
        ("o", "Open PDF (Browser)", act_open_pdf),
        ("v", "Open PDF Viewer", act_open_pdf_viewer),
        ("b", "Back", act_back),
    ]
    prompt_string = "Select Exercise [1-9A-F]  Open PDF (Browser) [o]  Open PDF Viewer [v]  Back [b]"

    while True:
        message = selected_group.group_string()+"\n\n"
        i = 1
        for e in range(state.frame.first_ex, state.frame.last_ex+1):
            message += "\t[{}] {}\n".format(hex(i)[2:].upper(), sheet.exercises[e-1].short_rep())
            i += 1
        state.sel_method = act_exercise_view
        act = handle_action(state,actions,message,prompt_string=prompt_string)
        if act == "b":
            state.frame.autosave()
            break

    state.sel_method = None
    state.selected_group = None
    
# ========== UTILITY ========== #

def suffix_string(csvname, suffix):
    return csvname.replace(".csv", "") + suffix + ".csv"

# ========== MAIN ========== #

def main():
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--exercises', default="{}-{}".format(DEFAULT_FIRSTEX, DEFAULT_LASTEX))
    parser.add_argument('-s', '--suffix')
    args = parser.parse_args()

    if args.suffix is not None:
        global CSV_BASENAME
        global CSV_PATTERN
        global AUTOSAVE_FILENAME
        AUTOSAVE_FILENAME = suffix_string(AUTOSAVE_FILENAME, args.suffix)
        CSV_BASENAME = suffix_string(CSV_BASENAME, args.suffix)
        CSV_PATTERN = suffix_string(CSV_PATTERN, args.suffix)

    m = re.search(EX_PATTERN, args.exercises)
    first_ex = m.groups(1)[0]
    last_ex = m.groups(1)[1]

    if int(first_ex) < 1 or int(last_ex) > 16:
        print("Exercises must be between 1 and 16, exiting")
        exit(1)

    # Create Frame
    f = Frame(first_ex, last_ex)

    f.autosave()

    # start main menu
    menu(f)

if __name__ == "__main__":
    main()    
